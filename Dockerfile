FROM jfloff/alpine-python:3.6

ARG BUILD_DATE
ARG VCS_REF

MAINTAINER mheppner
LABEL maintainer="mheppner" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.name="mheppner/discord-ps2" \
      org.label-schema.description="Discord bot for outfit-based Planetside 2 servers" \
      org.label-schema.vcs-url="https://gitlab.com/mheppner/discord-ps2" \
      org.label-schema.vcs-ref=$VCS_REF

ENV PYTHONUNBUFFERED=1
ENV BUILD_DIR=/app

RUN mkdir -p $BUILD_DIR
WORKDIR $BUILD_DIR
ADD ./ ./

RUN apk add libffi-dev \
    && pip install --no-cache-dir -r requirements.txt \
    && touch .env

CMD ["python3.6", "main.py"]
