import logging

import asyncio
import discord

from discord_ps2 import settings
from discord_ps2.api import PS2CensusClient, CensusException
from discord_ps2.client import client


logger = logging.getLogger(__name__)

# how often the online players should be checked, seconds
CHECK_FREQUENCY = 2 * 60


async def now_playing():
    """Changes the 'now playing'/presence status of the bot."""
    # wait until the client logs in
    await client.wait_until_ready()
    api = PS2CensusClient()

    while True:
        logger.debug('checking online outfit members...')

        try:
            total, members = await api.online_outfit_members()
        except CensusException as e:
            logger.error('failed to fetch PS2 API: {}'.format(e))

            # set the normal status
            await client.change_presence(game=discord.Game(
                name=settings.NOW_PLAYING))
        else:
            num_online = len(members)
            output = f'({num_online} / {total})'

            # set the status
            await client.change_presence(activity=discord.Game(
                name=f'{settings.NOW_PLAYING} {output}'))

        # sleep this task and let it loop around for the next check
        await asyncio.sleep(CHECK_FREQUENCY)


client.loop.create_task(now_playing())
