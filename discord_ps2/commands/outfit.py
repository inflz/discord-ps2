from datetime import datetime, timedelta
import logging
import random

from crontab import CronTab
from discord.ext import commands
import pytz

from discord_ps2 import settings
from discord_ps2.bot import bot
from discord_ps2.api import PS2CensusClient, CensusException


class Outfit(commands.Cog):
    """All outfit-related commands."""

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.census = PS2CensusClient()

    @commands.command(aliases=['on'])
    async def online(self, ctx, outfit_name: str=''):
        """see online users"""
        self.logger.info(f'[online] {outfit_name}')

        async with ctx.message.channel.typing():
            try:
                total, members = await self.census.online_outfit_members(
                    alias=outfit_name.strip())
            except CensusException as e:
                await ctx.send(str(e))
                return

            num_online = len(members)
            percent = round(float(num_online) / float(total) * 100, 2)
            output = f'{num_online} / {total} currently online ({percent}%):\n'

            for idx, member in enumerate(members):
                output += f'\t{idx + 1}) {member["character"]["name"]["first"]}\n'
            await ctx.send(output)

    @commands.command(aliases=['m'])
    async def members(self, ctx, outfit_name: str=''):
        """list all outfit members"""
        self.logger.info(f'[members] {outfit_name}')

        async with ctx.message.channel.typing():
            try:
                members = await self.census.outfit_members(
                    alias=outfit_name.strip())
            except CensusException as e:
                await ctx.send(str(e))
                return

            # sort descending by rank
            ordered = sorted(members, key=lambda k: (int(k['rank_ordinal']), k['character']['name']['first_lower']))

            output = 'Current members\n'
            for idx, m in enumerate(ordered):
                line = f'\t{idx + 1}) {m["character"]["name"]["first"]} ({m["rank"]})\n'

                if len(line) + len(output) >= 2000:
                    await ctx.send(output)
                    output = 'Continued:\n'

                output += line

            await ctx.send(output)

    @commands.command()
    async def iscool(self, ctx, name: str = ''):
        """check if a member is cool

        The player name is found and determined cool if they have been online
        within 12 hours. Outfit leaders and non-members are always losers.
        """
        self.logger.info(f'[iscool] {name}')
        is_cool = False

        if not name:
            await ctx.send('I need a player name to search for')
            return

        try:
            players = await self.census.characters([name])
        except CensusException as e:
            await ctx.send(str(e))
            return

        if len(players) != 1:
            await ctx.send('That character does not exist')
            return
        player = players[0]

        # non outfit members default to not being cool
        if 'outfit' in player:
            if player['outfit']['leader_character_id'] == player['character_id']:
                # always set leaders as not cool
                is_cool = False
                self.logger.debug(f'{name} is an outfit leader')
            elif player['outfit']['outfit_id'] != settings.OUTFIT_ID:
                # non outfit members are definitely losers
                await ctx.send(f'{name} doesnt even go here')
                self.logger.debug(f'{name} is not in this outfit')
                return
            else:
                # get the last login time
                last_login = datetime.fromtimestamp(int(player['times']['last_login']))
                self.logger.debug(f'{name} last logged in on {last_login}')

                # only cool kids play ps2 all the time
                is_cool = last_login >= datetime.now() - timedelta(hours=12)

        # say a random status
        choices = (settings.ISCOOL_POSITIVE_RESPONSES
                   if is_cool
                   else settings.ISCOOL_NEGATIVE_RESPONSES)
        choice = random.choice(choices)
        await ctx.send(choice.format(name))

    @commands.command(aliases=['d'])
    async def deaths(self, ctx, outfit_name: str=''):
        """mourn recent outfit deaths

        Lists the most recent death events for online members of an outfit.
        """
        self.logger.info('[deaths]')

        try:
            # only look at online members
            _, members = await self.census.online_outfit_members(alias=outfit_name)

            # get only the character ids
            ids = [x['character_id'] for x in members]

            # find death events for the online players
            events = await self.census.characters_events(ids, 'DEATH')
        except CensusException as e:
            await ctx.send(str(e))
            return

        if len(events):
            output = '☠️ RIP these heroes:\n'
            for event in events[:20]:
                output += f'\t{event["character"]["name"]["first"]}\n'
            await ctx.send(output)
        else:
            await ctx.send('No one has died recently.')

    @commands.command()
    async def ops(self, ctx, timezone: str=settings.TIMEZONE):
        """show when the next outfit ops occurs"""
        self.logger.info(f'[ops] {timezone}')

        if not len(settings.OUTFIT_OPS):
            await ctx.send('There are no ops planned 😑')
            return

        # find the correct timezone
        for tz in pytz.all_timezones:
            if tz.lower() == timezone.lower():
                timezone = tz
                break
        else:
            await ctx.send('That is not a valid timezone')
            return

        now = pytz.timezone('UTC').localize(datetime.utcnow())
        ops = []

        for cron in settings.OUTFIT_OPS:
            # parse the crontab
            entry = CronTab(cron)

            # get the next start time in seconds
            start_sec = entry.next(now)
            ops.append(start_sec)

        # sort asc
        ops = sorted(ops)

        # the first item is the next event
        # use the parsed seconds to get the actual start dates
        start = now + timedelta(seconds=ops[0])

        # convert to a nice string
        tz = pytz.timezone(timezone).normalize(start)
        start_str = tz.strftime('%a %b %d at %I:%M %p %Z')
        await ctx.send(f'Next ops is {start_str}')


bot.add_cog(Outfit(bot))
