from collections import OrderedDict
import logging
from operator import itemgetter

from discord.ext import commands

from discord_ps2.bot import bot
from discord_ps2.api import helper
from discord_ps2.api import (PS2CensusClient, CensusException)
from discord_ps2.utils import convert_timestamp
from discord_ps2 import settings


class Statistics(commands.Cog):
    """All commands related to getting stats on players or outfits."""

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.census = PS2CensusClient()

    @commands.command(aliases=['fp'])
    async def findplayer(self, ctx, name: str = '', max_events: int = 10):
        """find where a player is fighting.

        The facility capture/defense events are found for the user.
        """
        self.logger.info(f'[findplayer] {name} {max_events}')

        if not len(name):
            await ctx.send('I need a character name to search for')
            return

        try:
            characters = await self.census.characters([name])
        except CensusException as e:
            await ctx.send(str(e))
            return

        if len(characters) != 1:
            await ctx.send('That character does not exist')
            return
        character = characters[0]

        last_login = convert_timestamp(character['times']['last_save'])

        try:
            events = await self.census.character_events(
                character['character_id'], limit=int(max_events))
        except CensusException as e:
            await ctx.send(str(e))
            return

        output = '{} was last seen {} and was found here:\n'.format(
            character['name']['first'], last_login.strftime('%Y/%m/%d %I:%M %p %Z'))

        for event in events:
            time = convert_timestamp(event['timestamp'])
            continent = helper.zones[event['zone_id']]
            base = helper.find_base_by_facility(event['zone_id'], event['facility_id'])
            status = 'defended' if event['event_type'] == 'PlayerFacilityDefend' else 'captured'

            output += '{} - {} **{}** ({})\n'.format(
                time.strftime('%Y/%m/%d %I:%M %p'), status,
                base['facility_name'], continent['name']['en']
            )
        await ctx.send(output)

    @commands.command(aliases=['fo'])
    async def findoutfit(self, ctx, alias: str = '', max_times: int = 10):
        """find where an outfit is playing at

        The outfit members and all of their facility capture/defense events are fetched. The
        events are rounded to nearest 5 minute intervals, since it's a fairly average value
        for how long it may take to capture a base.
        """
        self.logger.info(f'[findoutfit] {alias} {max_times}')
        alias = alias.strip()

        if not alias:
            await ctx.send('I need an outfit alias to search for')
            return

        # display a loading message, will be deleted later
        loading_msg = await ctx.send('Deploying motion spotters...')
        async with ctx.message.channel.typing():

            # get the outfit info first
            try:
                outfit = await self.census.outfit(alias)
            except CensusException as e:
                await ctx.send(str(e))
                return

            # get list of outfit members, joined with character events
            try:
                params = {
                    'c:resolve': '',
                    'c:join': ('type:characters_event^on:character_id'
                               '^terms:type=FACILITY_CHARACTER^list:1'
                               '^inject_at:events^outer:0')
                }
                members = await self.census.outfit_members(id=outfit['outfit_id'],
                                                           params=params)
            except CensusException as e:
                await ctx.send(str(e))
                return

            # remove loading message
            await loading_msg.delete()

            # round timestamps up or down to fuzz results
            ROUND_MINUTES = 5
            # keep track of events, with timestamps used as keys
            events = {}
            # basis for what bot will say
            output = f'{outfit["name"]} members were found here:\n'
            # keeps track of displayed timestamps
            num_displayed = 0

            # build a list of events from each outfit member
            for member in members:
                if 'events' not in member:
                    continue

                for event in member['events']:
                    # create new timestamps rounded to nearest 5 minute intervals
                    timestamp_rounded = ROUND_MINUTES * 60 * \
                        (round(int(event['timestamp']) / (ROUND_MINUTES * 60)))

                    # put events into buckets based on rounded timestamps
                    if timestamp_rounded in events:
                        events[timestamp_rounded].append(event)
                    else:
                        events[timestamp_rounded] = [event]

            if not len(events.keys()):
                await ctx.send(f'I cannot find any recent events for {outfit["name"]}')
                return

            # sort events by timestamp
            sorted_events = OrderedDict(sorted(events.items(), key=lambda t: t[0], reverse=True))

            # loop through and display the most recent num_displayed events
            for timestamp in sorted_events:
                # limit the number of displayed events
                num_displayed += 1
                if num_displayed > max_times:
                    break

                local_events = sorted_events[timestamp]
                # used to organize the facilities of the events in this timestamp bucket
                facilities = {}
                # convert the timestamp to a datetime
                dt = convert_timestamp(timestamp)
                output += '\t{}\n'.format(dt.strftime('%a %b %d %I:%M %p'))

                # loop through events at this timestamp and group into the facilities
                for event in local_events:
                    if event['facility_id'] in facilities:
                        facilities[event['facility_id']].append(event)
                    else:
                        facilities[event['facility_id']] = [event]

                # sort the facilities by most common first
                sorted_facilities = OrderedDict(
                    sorted(facilities.items(), key=lambda t: len(t[1]), reverse=True))

                # loop through the facilities and display them
                for facility_id in sorted_facilities:
                    facility_events = sorted_facilities[facility_id]
                    # facilities are already grouped, so just grab the zone_id from the first one
                    zone_id = facility_events[0]['zone_id']
                    # lookup continent and base
                    continent = helper.zones[zone_id]
                    base = helper.find_base_by_facility(zone_id, facility_id)

                    output += (
                        f'\t\t{len(facility_events)} '
                        f'at **{base["facility_name"]}** ({continent["name"]["en"]})\n')

            await ctx.send(output)


bot.add_cog(Statistics(bot))
