import asyncio
import logging
import random

from colour import COLOR_NAME_TO_RGB, Color
from discord.ext import commands

from discord_ps2.bot import bot
from discord_ps2 import settings


class Extra(commands.Cog):
    """Miscellaneous commands."""

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    @commands.command()
    async def textcolor(self, ctx, color: str = '', *, msg: str = ''):
        """create a colored text for squad and platoon names"""
        self.logger.info(f'[textcolor] {color} {msg}')

        color_options = COLOR_NAME_TO_RGB.keys()
        if color not in color_options:
            await ctx.send(
                f'Invalid color name! Try one of these:\n{",".join(color_options)}')
            return

        await ctx.send(f'```html\n<font color="{Color(color).hex}">{msg}</font>\n```')

    @commands.command()
    async def rtd(self, ctx):
        """rolls the dice"""
        self.logger.info('[rtd]')
        choice = random.randint(1, 6)
        await ctx.send(f'🎲 {choice}')

    @commands.command()
    async def rps(self, ctx, choice: str=''):
        """play rock paper scissors"""
        self.logger.info(f'[rps] {choice}')
        choice_clean = choice.strip().lower()
        options = {
            'rock': {
                'display': '🗿',
                'beats': ('paper', 'scissors')
            },
            'paper': {
                'display': '📝',
                'beats': ('rock',)
            },
            'scissors': {
                'display': '✂️',
                'beats': ('paper',)
            }
        }

        # make sure they picked a valid option
        if choice_clean not in options.keys():
            await ctx.send('have you ever played this before?')
            return

        # let the bot pick
        my_choice_name = random.choice(list(options.keys()))
        my_choice = options[my_choice_name]

        output = f'\n{my_choice["display"]} {my_choice_name} - '

        if choice_clean in my_choice['beats']:
            # bot beat user's choice
            output += "you're fake news!"
        elif choice_clean == my_choice_name:
            # its a tie
            output += "let's play again"
        else:
            # bot lost :(
            output += 'you got me this time'

        await ctx.send(output)

    @commands.command(hidden=True)
    async def clearchat(self, ctx, num: str='1'):
        """deletes chat messages from this channel"""
        self.logger.info(f'[clearchat] {num}')
        try:
            n = int(num)
        except ValueError:
            await ctx.send('is that a number?')
            return

        # check if the user is an admin
        is_admin = False
        for name, status in ctx.message.channel.permissions_for(ctx.message.author):
            if name == 'administrator' and status:
                is_admin = True

        if not is_admin:
            await ctx.send('Sorry, only admins can use this')
            return

        # delete the last n messages
        deleted = await ctx.message.channel.purge(limit=n)

        # show a message and delete it
        status_msg = await ctx.send(f'Deleted {len(deleted)} messages')
        await asyncio.sleep(5)
        await self.bot.delete_message(status_msg)


bot.add_cog(Extra(bot))
