"""Main definition for the bot object."""
import logging

import discord
from discord.ext import commands

from . import settings
from .api import helper


logger = logging.getLogger(__name__)

bot = commands.Bot(
    command_prefix=settings.BOT_PREFIX,
    description=settings.BOT_DESCRIPTION
)


@bot.event
async def on_ready():
    logger.info('logged in as {} ({})'.format(bot.user.name, bot.user.id))

    # load static items into the API helper
    await helper.load_defaults()
