import logging

from aiohttp import ClientSession, ClientError
from aiocache import cached

from discord_ps2 import settings


session = ClientSession(raise_for_status=True, conn_timeout=30)


class FISUException(Exception):
    """Any FISU client error."""

    pass


class FISUClient(object):

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    async def _fetch(self, url: str, params: dict = None):
        """Fetch a URL with params using the internal session.

        :param url: the url to GET
        :param params: any GET params to attach to the request
        """
        try:
            async with session.get(url, params=params) as res:
                data = await res.json()
        except ClientError as e:
            self.logger.error('failed to fetch FISU API', e)
            raise FISUException(e)
        else:
            return data

    @cached(alias='default', ttl=60 * 5)
    async def world_population(self, id: str, params: dict = None):
        query = {
            'world': id
        }
        if params is not None:
            query.update(params)
        url = settings.FISU_BASE_URL + 'population'

        data = await self._fetch(url, query)
        if len(data['result']):
            return data['result'][0]
        raise FISUException('That world does not exist')
