# zone_ids/continents, excluding training facilities
CONTINENT_ZONES = ['2', '4', '6', '8']

# warpgates region_ids, mapped by zone_ids/continents
WARPGATES = {
    # indar: N, W, E
    '2': ['2201', '2202', '2203'],
    # hossin: W, E, S
    '4': ['4230', '4240', '4250'],
    # amerish: W, E, S
    '6': ['6001', '6002', '6003'],
    # esamir: N, S, E
    '8': ['18029', '18030', '18031'],
}

# map a metagame_event_id to a zone_id/continent
ALERTS = {
    # indar
    '1': '2',
    '10': '2',
    '11': '2',
    '12': '2',
    '123': '2',
    '124': '2',
    '125': '2',
    '147': '2',
    '148': '2',
    '149': '2',
    # esamir
    '2': '8',
    '13': '8',
    '14': '8',
    '126': '8',
    '127': '8',
    '128': '8',
    '150': '8',
    '151': '8',
    '152': '8',
    '164': '8',
    # amerish
    '3': '6',
    '7': '6',
    '8': '6',
    '9': '6',
    '132': '6',
    '133': '6',
    '134': '6',
    '156': '6',
    '157': '6',
    '158': '6',
    # hossin
    '4': '4',
    '16': '4',
    '17': '4',
    '18': '4',
    '129': '4',
    '130': '4',
    '131': '4',
    '153': '4',
    '154': '4',
    '155': '4',
}

FACTIONS = {
    '0': 'Nanite Systems',
    '1': 'Vanu Sovereignty',
    '2': 'New Conglomerate',
    '3': 'Terran Republic'
}

FACTIONS_SHORT = {
    '0': 'Nanite',
    '1': 'VS',
    '2': 'NC',
    '3': 'TR',
}
