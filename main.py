#!/usr/bin/env python
import argparse
import asyncio
import logging

from discord_ps2.bot import bot
from discord_ps2.client import client
from discord_ps2 import settings


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Start a Planetside 2 Discord bot.')
    parser.add_argument(
        '-d', '--debug',
        help='Enable DEBUG logging',
        action='store_const', dest='loglevel', const=logging.DEBUG,
        default=logging.WARNING)
    parser.add_argument(
        '-v', '--verbose',
        help='Enable INFO logging',
        action='store_const', dest='loglevel', const=logging.INFO)

    # parse arguments and convert to dict
    opts = vars(parser.parse_args())

    # grab the loglevel and set up a root logger
    loglevel = opts.pop('loglevel')
    logging.basicConfig(level=loglevel)

    # gather the bot and client tasks into the same loop
    loop = asyncio.get_event_loop()
    bot_task = loop.create_task(bot.start(settings.DISCORD_AUTH_TOKEN))
    client_task = loop.create_task(client.start(settings.DISCORD_AUTH_TOKEN))
    gathered = asyncio.gather(bot_task, client_task, loop=loop)

    # run both the client and the bot
    loop.run_until_complete(gathered)
