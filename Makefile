PROJECT=discord_ps2

FLAKE8=flake8
PYDOCSTYLE=pydocstyle
DOCKER=docker
GIT=git
DATE=date

DOCKER_IMAGE=mheppner/discord-ps2
DOCKER_TAG=latest

all: help

help:
	@echo "clean -------------- - Clean all artifacts."
	@echo "  clean-pyc          - Remove Python cache files."
	@echo "distcheck ---------- - Check distribution for problems."
	@echo "  flake              - Run flake8 on the source code."
	@echo "  docscheck          - Run pydocstyle on Python files."
	@echo "docker ------------- - Build and push the docker image."
	@echo "  build              - Builds and versions the docker image."
	@echo "  push               - Pushes the docker images to the repo."

clean: clean-pyc

clean-pyc:
	-find . -type f -a \( -name "*.pyc" -o -name "*$$py.class" \) | xargs rm
	-find . -type d -name "__pycache__" | xargs rm -r

distcheck: flake docscheck

flake:
	$(FLAKE8) "$(PROJECT)"

docscheck:
	$(PYDOCSTYLE) "$(PROJECT)"

docker: build push

build:
	$(DOCKER) build \
		--build-arg BUILD_DATE=`$(DATE) -u +'%Y-%m-%dT%H:%M:%SZ'` \
		--build-arg VCS_REF=`$(GIT) rev-parse HEAD` \
		-t "$(DOCKER_IMAGE):$(DOCKER_TAG)" .

push:
	$(DOCKER) push "$(DOCKER_IMAGE):$(DOCKER_TAG)"
